const autoprefixer = require('autoprefixer')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')
const webpack = require('webpack')
const IS_DEV = require('isDev');



const config = {
  entry: {
    app: './src/index'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg|wav|mp3)(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: "file-loader" },
      { test: /\.(png|jpg|jpeg|gif)$/, loader: 'url-loader?limit=10000' },
      { test: /\.(eot|ttf|wav|mp3)$/, loader: 'file-loader' },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1',
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          },
          'sass-loader'
        ]
      }

    ]
  },
  devServer: {
    historyApiFallback: true,
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/'
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    new webpack.LoaderOptionsPlugin({
         test: /\.css$/, // may apply this only for some modules
         options: {
           postcss: [
             autoprefixer({
               browsers: ['last 2 versions']
             })
           ]
         }
       })
  ],

  resolve: {
    extensions: ['.js', '.scss'],
    modules: [
      path.join(__dirname, './src'),
      'node_modules'
    ]
  }
}

module.exports = config;
