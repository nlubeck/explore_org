# Explore - React SPA
Decided to convert explore.org into a responsive SPA utilizing the latest web technologies.

## Stack
 - React 15
 - Bootstrap 4
 - Webpack 2
 - PostCSS

## How To Get Started
Install `node`, run `npm i`

## Start Development
Running `npm run start:dev` will spool up webpack-dev-server
Code away! :beer:

## Production
Running `npm run build` will minify CSS/JS.


###TODOS
Implement redux and tests
