// import 'stylesheets/base'

import React from 'react'
import ReactDOM from 'react-dom'
import AppContainer from 'javascripts/AppContainer'
import LiveCams from 'javascripts/containers/LiveCams'
import Films from 'javascripts/containers/Films'
import Photos from 'javascripts/containers/Photos'
import Grants from 'javascripts/containers/Grants'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'

ReactDOM.render( <Router history={browserHistory}>
    <Route path="/" component={AppContainer}>
      <IndexRoute component={LiveCams} />
      <Route path="/live-cams" component={LiveCams}/>
      <Route path="/films" component={Films}/>
      <Route path="/photos" component={Photos}/>
      <Route path="/grants" component={Grants}/>
    </Route>
  </Router>, document.querySelector('#app'))
