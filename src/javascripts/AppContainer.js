import React from 'react'
import Header from 'javascripts/components/header'
import Footer from 'javascripts/components/footer'

import 'stylesheets/modules/base/base.css'

const defaultProps = {
  themeName: 'light-green',
  theme: false
};

const propTypes = {
  theme: React.PropTypes.bool,
  themeName: React.PropTypes.string
};
const AppContainer = React.createClass({
  render () {
    const {theme, themeName} = this.props;
    let className = (!theme) ? '' : 'theme-' + themeName;
    return (
      <div className={className}>
        <Header />
          <div className="container mt-2 mb-2">
            {this.props.children}
          </div>

        <Footer />
      </div>
    )
  }
})
AppContainer.defaultProps = defaultProps;
AppContainer.propTypes = propTypes;
export default AppContainer
