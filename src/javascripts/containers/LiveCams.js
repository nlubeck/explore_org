import React from 'react'
import YoutubePlayer from 'react-youtube-player';


const defaultProps = {};
const propTypes = {};

const LiveCams = React.createClass({
  render () {

    return (
      <div className="row">
        <div className="col-sm-12">
          <div className="rounded bg-fff container-boxShadow p-3">
            <YoutubePlayer videoId='rxbmfJkfMdM' playbackState='playing' configuration={{showinfo: 0, controls:0}} height={'450'} />
          </div>
        </div>
      </div>
    )
  }
})

LiveCams.defaultProps = defaultProps;
LiveCams.propTypes = propTypes;
export default LiveCams
