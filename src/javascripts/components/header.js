import React from 'react'
import { Link, IndexLink } from 'react-router'

import 'stylesheets/modules/header/header.css'
// import 'stylesheets/utilities/clearfix'

const Header = React.createClass({
  render () {
    return (
      <div className='header u-clearfix'>
        <div className="container pt-4 pb-3">
          <div className="row">
            <div className="col-6 col-md-3">
                <IndexLink to="/" title="explore"><i className="logo"></i></IndexLink>
            </div>
            <div className="col-6 col-md-3 offset-md-1">
              <form name="search-form">
                <div className="input-group pt-3 pb-3">
                  <input type="text" className="form-control" placeholder="Search for..." />
                  <span className="input-group-btn">
                    <button className="btn btn-secondary" type="button"><i className="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
            </div>

            <div className="col-md-4 offset-md-1">
              <a className="blog-link" href="https://blog.explore.org/the-powerpuff-girls-cerebellar-hypoplasia/" title="The Powerpuff Girls & Cerebellar Hypoplasia" target="_blank">
              <div className="row">
                <div className="col-sm-4 hidden-sm-down">
                  <img className="img-fluid" src="https://blog.explore.org/wp-content/uploads/2017/01/WobbliesInWheels-300x200.jpg" alt="The Powerpuff &amp; Cerrebellar Hypoplasia" />
                </div>
                <div className="col-sm-8 u-textSize-14 pl-sm-0">
                  <p className="mt-0 mb-0 blog-callout">Blog Post</p>
                  The Powerpuff Girls &amp; Cerrebellar Hypoplasia
                </div>
              </div>
              </a>
            </div>
          </div>
        </div>
        <div className="secondary-header u-clearfix">
          <div className="container">
          <div className="row no-gutters">
            <div className="col">
              <ul className="nav">
                <li className="nav-item">
                  <Link className="nav-link active" to="/live-cams">live cams</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/films">films</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/photos">photos</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/blog">blog</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/grants">grants</Link>
                </li>
              </ul>
            </div>
            <div className="justify-end">
              <ul className="nav">
                <li className="nav-item border-left-2">
                  <a className="nav-link active" href="/signup">signup</a>
                </li>
                <li className="nav-item  border-left-2">
                  <a className="nav-link" href="/login">login</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/feedback">feedback</a>
                </li>
              </ul>
            </div>
          </div>
          </div>
        </div>
      </div>
    )
  }
})

export default Header
