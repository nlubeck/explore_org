import React from 'react'

import 'stylesheets/modules/footer/footer.css'

const Footer = React.createClass({
  render () {
    return (
      <div className='footer mt-3'>
      <div className="footer-callout pt-2 pb-3">
        <div className="container">
          <div className="row">
            <div className="col">
              <span className="lead-text">explore</span> mission: <br/> to champion the selfless acts of others, create a portal into the soul of humanity and inspire lifelong learning. <span className="lead-text">Made possible by the Annenberg Foundation.</span>
            </div>
          </div>
        </div>
      </div>
      <div className="container pt-3 pb-3">
        <div className="row">
          <div className="col-sm-3">
            <nav className="nav flex-column">
            <span className="legal-text nav-link">&copy; 2017 Explore Annenberg LLC<br/>All Rights Reserved</span>
            <a className="nav-link" href="/extra/privacy-policy/" title="Privacy Policy">Privacy Policy</a>
            <a className="nav-link" href="/extra/terms-of-use/" title="Terms Of Use">Terms of Use</a>
            </nav>
          </div>
          <div className="col-xs-6 col-sm-3">
            <nav className="nav flex-column">
              <a className="nav-link" href="/about/who-we-are" title="About Us">About Us</a>
              <a className="nav-link" href="/contact-us" title="Contact Us">Contact Us</a>
              <a className="nav-link" href="/feedback" title="Feedback">Feedback</a>
              <a className="nav-link" href="/community-guidelines" title="Community Guidelines">Community Guidelines</a>
              <a className="nav-link" href="http://unjustus.org" title="UNJUSTUS">UNJUSTUS</a>
            </nav>
          </div>
          <div className="col-xs-6 col-sm-3">
            <nav className="nav flex-column">
              <a className="nav-link" href="/education" title="Education">Education</a>
              <a className="nav-link" href="/grants" title="Grants">Grants</a>
              <a className="nav-link" href="/interviews" title="Interviews">Interviews</a>
              <a className="nav-link" href="/photo-archive" title="Photo Archive">Photo Archive</a>
              <a className="nav-link" href="/mobile-apps" title="Mobile Apps">Mobile Apps</a>
            </nav>
          </div>
          <div className="col-sm-3">
            <span className="lead-text">follow</span> explore
            <nav className="nav">
                <a className="nav-link" href="https://facebook.com/endageredanimals" title="Like Us on Facebook"><i className="fa fa-facebook fa-2x"></i></a>
                <a className="nav-link" href="https://twitter.com/exploreorg" title="Follow Us on Twitter"><i className="fa fa-twitter fa-2x"></i></a>
                <a className="nav-link" href="https://www.youtube.com/user/exploreTeam" title="Subscribe to YouTube"><i className="fa fa-youtube fa-2x"></i></a>
                <a className="nav-link" href="https://vimeo.com/teamexplore" title="Vimeo"><i className="fa fa-vimeo fa-2x"></i></a>
                <a className="nav-link" href="http://explore.org/rss" title="Subscribe to our RSS Feed"><i className="fa fa-rss fa-2x"></i></a>
            </nav>
            <div className="newsletter">
                <span className="lead-text">subscribe to</span> explore updates
                <div className="input-group">
                  <input type="text" className="form-control" placeholder="enter email" />
                  <span className="input-group-btn">
                    <button className="btn btn-secondary" type="button"><i className="fa fa-fw fa-arrow-circle-right"></i></button>
                  </span>
                </div>

            </div>
          </div>
        </div>

      </div>
      </div>
    )
  }
})

export default Footer
